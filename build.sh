#!/bin/sh

set -xe

gcc -Wall -Wextra -Wpedantic -O2 -o rpnc rpnc.c
