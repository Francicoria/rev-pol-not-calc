// @todo implement negative numbers

#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

//#define BUMP_ALLOCATOR_LOG

#define report_fmt(level, s, ...) fprintf(stderr, "[" #level "]: " s "\n", __VA_ARGS__)
#define report(level, s) fprintf(stderr, "[" #level "]: " s "\n")

#define node_number_fmt "%.2f"
typedef float node_number_t;

static inline bool is_node_number_zero(node_number_t n) {
	return n == 0;
}

static inline node_number_t node_number_abs(node_number_t n) {
	return fabsf(n);
}

typedef struct Node_ {
	enum Node_Type {
		NT_NUMBER = 0,
		NT_UNOP,
		NT_BINOP,
	} node_type;

	union Node_Value {
		node_number_t number;

		struct Unop {
			enum Unop_Type {
				UNOP_ABS = 0,
			} type;

			struct Node_ *op1;
		} unop;

		struct Binop {
			enum Binop_Type {
				BINOP_ADD = 0,
				BINOP_SUB,
				BINOP_MUL,
				BINOP_DIV,
			} type;

			struct Node_ *op1;
			struct Node_ *op2;
		} binop;
	} node_value;
} Node;

static inline void print_ident(size_t n) {
	for (size_t i = 0; i < n; ++i) printf("   ");
}

static void print_node(Node n, size_t ident) {
	print_ident(ident);
	switch (n.node_type) {
	case NT_NUMBER: {
		printf("NUMBER("node_number_fmt")\n", n.node_value.number);
	} break;
	case NT_UNOP: {
		printf("UNOP(\n");
		print_node(*n.node_value.unop.op1, ident + 1);
		print_ident(ident);
		printf(")\n");
	} break;
	case NT_BINOP: {
		printf("BINOP(\n");
		print_node(*n.node_value.binop.op1, ident + 1);
		print_node(*n.node_value.binop.op2, ident + 1);
		print_ident(ident);
		printf(")\n");
	} break;
	}
}

#define BUMP_DATA_CAP (32 * 1024)
struct {
	uint8_t data[BUMP_DATA_CAP];
	size_t cursor;
} bump;
static void *bump_alloc_(size_t n, const char *file_loc, int line_loc) {
#ifdef BUMP_ALLOCATOR_LOG
	report_fmt(INFO, "Allocated %zu bytes at %s:%d", n, file_loc, line_loc);
#endif
	if (bump.cursor + n >= BUMP_DATA_CAP) {
		report_fmt(ALLOCATOR, "Exceeded bump allocator capacity at %s:%d", file_loc, line_loc);
		exit(1);
	}
	void *ptr = &bump.data[bump.cursor];
	bump.cursor += n;

	return ptr;
}
#define bump_alloc(n) bump_alloc_(n, __FILE__, __LINE__)

static inline void bump_reset(void) {
	bump.cursor = 0;
}

static inline Node *create_empty_node(void) {
	Node *n = bump_alloc(sizeof(Node));
	memset(n, 0, sizeof(Node));
	return n;
}

static Node *create_number_node(node_number_t value) {
	Node *n = create_empty_node();
	n->node_type = NT_NUMBER;
	n->node_value.number = value;

	return n;
}

static Node *create_unop_node(enum Unop_Type type, Node *op1) {
	assert(op1);

	Node *n = create_empty_node();
	n->node_type = NT_UNOP;
	n->node_value.unop.type = type;
	n->node_value.unop.op1 = op1;

	return n;
}

static Node *create_binop_node(enum Binop_Type type, Node *op1, Node *op2) {
	assert(op1 && op2);

	Node *n = create_empty_node();
	n->node_type = NT_BINOP;
	n->node_value.binop.type = type;
	n->node_value.binop.op1 = op1;
	n->node_value.binop.op2 = op2;

	return n;
}

static node_number_t execute_node(Node *n) {
	assert(n);

	switch (n->node_type) {
	case NT_NUMBER: {
		return n->node_value.number;
	} break;
	case NT_UNOP: {
		struct Unop uop = n->node_value.unop;
		switch (uop.type) {
		case UNOP_ABS: {
			return node_number_abs(execute_node(uop.op1));
		}
		}
	} break;
	case NT_BINOP: {
		struct Binop bop = n->node_value.binop;
		switch (bop.type) {
		case BINOP_ADD: {
			return execute_node(bop.op1) + execute_node(bop.op2);
		} break;
		case BINOP_SUB: {
			return execute_node(bop.op1) - execute_node(bop.op2);
		} break;
		case BINOP_MUL: {
			return execute_node(bop.op1) * execute_node(bop.op2);
		} break;
		case BINOP_DIV: {
			node_number_t n_op2 = execute_node(bop.op2);
			if (is_node_number_zero(n_op2)) {
				report(ERROR, "Attempted division by zero");
				exit(1);
			}
			return execute_node(bop.op1) / n_op2;
		} break;
		}
	} break;
	}
	assert(false && "unreachable");
}

#define ARRAY_LEN(xs) (sizeof((xs))/sizeof(*(xs)))

typedef struct {
	const char *str;
	size_t len;
} StrV;
#define StrV_fmt "%.*s"
#define StrV_printf_arg(s) (int)s.len, s.str

static inline StrV strv_from_cstr(const char *cstr) {
	return (StrV) {
		.str = cstr,
		.len = strlen(cstr),
	};
}

static bool strv_eq(StrV s1, StrV s2) {
	if (s1.len != s2.len) return false;

	for (size_t i = 0; i < s1.len; ++i) if (s1.str[i] != s2.str[i]) return false;

	return true;
}

static StrV strv_chop(StrV *tail, char delimiter) {
	StrV head = {
		.str = tail->str,
		.len = 0,
	};
	while (tail->len > 0) {
		if (*tail->str == delimiter) {
			tail->str += 1;
			tail->len -= 1;
			return head;
		}

		head.len += 1;
		tail->str += 1;
		tail->len -= 1;
	}

	return head;
}

static inline bool is_whitespace(char c) {
	return c == ' ' || c == '\t' || c == '\n';
}

static StrV strv_trim_left_spaces(StrV s) {
	while (s.len > 0 && is_whitespace(*s.str)) {
		s.str += 1;
		s.len -= 1;
	}
	return s;
}

static StrV strv_trim_right_spaces(StrV s) {
	while (s.len > 0 && is_whitespace(s.str[s.len-1])) {
		s.len -= 1;
	}
	return s;
}

static StrV strv_trim_spaces(StrV s) {
	s = strv_trim_left_spaces(s);
	s = strv_trim_right_spaces(s);
	return s;
}

// @todo implement negative numbers
static bool strv_parse_integer(StrV s, int *result) {
	for (size_t i = 0; i < s.len; ++i) {
		char c = s.str[i];
		if (c < '0' || c > '9') return false;
		*result *= 10;
		*result += c - '0';
	}
	return true;
}

typedef struct {
	enum Token_Type {
		TT_NUMBER = 0,
		TT_IDENT,
		TT_PLUS = '+',
		TT_MINUS = '-',
		TT_ASTERISK = '*',
		TT_SLASH = '/',
	} type;
	union Token_Value {
		int number;
		StrV ident;
	} value;
} Token;

#define TOKENS_CAP (128)
typedef struct {
	size_t len;
	Token *elems;
} Tokens;

static Token *emplace_token(Tokens *ts) {
	if (ts->len + 1 >= TOKENS_CAP) {
		report(ERROR, "Exceeded tokens capacity");
		exit(1);
	}
	return &ts->elems[ts->len++];
}

static void debug_print_tokens(Tokens tokens) {
	for (size_t i = 0; i < tokens.len; ++i) {
		Token token = tokens.elems[i];
		switch (token.type) {
		case TT_NUMBER: {
			printf("number <%d>\n", token.value.number);
		} break;
		case TT_IDENT: {
			printf("identifier <\"" StrV_fmt "\">\n", StrV_printf_arg(token.value.ident));
		} break;
		case TT_PLUS: { printf("plus [+]\n"); } break;
		case TT_MINUS: { printf("minus [-]\n"); } break;
		case TT_ASTERISK: { printf("asterisk [*]\n"); } break;
		case TT_SLASH: { printf("slash [/]\n"); } break;
		}
	}
}

#define NODE_STACK_CAP (128)
typedef struct {
	size_t len;
	Node **elems;
} Node_Stack;
static void node_stack_push(Node_Stack *ns, Node *elem) {
	if (ns->len + 1 >= NODE_STACK_CAP) {
		report(ERROR, "Exceeded node stack capacity");
		exit(1);
	}
	ns->elems[ns->len++] = elem;
}

static Node *node_stack_pop(Node_Stack *ns) {
	if (ns->len == 0) {
		report(ERROR, "Trying to pop when node stack is empty");
		exit(1);
	}
	return ns->elems[--ns->len];
}

static StrV get_entire_file_content(const char *filepath) {
	FILE *f = fopen(filepath, "rb");
	char *content = NULL;
	if (!f) {
		report_fmt(IO, "Couldn't open file %s for reading", filepath);
		goto err;
	}
	if (fseek(f, 0, SEEK_END)) goto err;

	long len = ftell(f);
	if (len <= 0) goto err;

	if (fseek(f, 0, SEEK_SET)) goto err;

	content = (char *)malloc(len);
	if (!content) goto err;

	if (fread(content, 1, len, f) < (size_t)len) {
		report_fmt(IO, "Couldn't read from file %s", filepath);
		goto err;
	}

	return (StrV) {
		.str = (const char *)content,
			.len = len,
	};

err:
	if (f) fclose(f);
	if (content) free(content);
	return (StrV) {0};
}

static bool lex(StrV text, Tokens *tokens) {
	while (text.len > 0) {
		StrV head = strv_chop(&text, ' ');
		text = strv_trim_spaces(text);

		Token *t = emplace_token(tokens);

		if (head.len == 1) {
			char operator = head.str[0];
			switch (operator) {
			case '+':
			case '-':
			case '*':
			case '/': {
				t->type = operator;
				continue;
			}
			}
		}
		int maybe_integer = 0;
		if (strv_parse_integer(head, &maybe_integer)) {
			t->type = TT_NUMBER;
			t->value.number = maybe_integer;
			continue;
		}

		t->type = TT_IDENT;

		StrV head_copy = {0};
		head_copy.len = head.len;
		head_copy.str = bump_alloc(head.len);
		memcpy((void *)head_copy.str, head.str, head.len);

		t->value.ident = head_copy;
	}

	return true;
}

int main(int argc, char **argv) {
	const char *program_name = argv[0];
	if (argc < 2) {
		report(ERROR, "input file argument wasn't provided");
		report_fmt(USAGE, "%s <input.rpnc>", program_name);
		return 1;
	}
	const char *file_path = argv[1];

	StrV text = get_entire_file_content(file_path);
	if (text.len == 0) {
		report_fmt(ERROR, "File %s is empty", file_path);
		goto err;
	}
	StrV trimmed = strv_trim_spaces(text);

	if (trimmed.len == 0) {
		report_fmt(ERROR, "File %s is empty", file_path);
		goto err;
	}

	Tokens tokens = {0};
	tokens.elems = (Token *)bump_alloc(TOKENS_CAP * sizeof(Token));
	if (!lex(trimmed, &tokens)) goto err;

	free((void *)text.str);

	//debug_print_tokens(tokens);

	Node_Stack node_stack = {0};
	node_stack.elems = bump_alloc(NODE_STACK_CAP * sizeof(Node));
	for (size_t i = 0; i < tokens.len; ++i) {
		Token token = tokens.elems[i];
		switch (token.type) {
		case TT_NUMBER: {
			Node *n = create_number_node(token.value.number);
			node_stack_push(&node_stack, n);
		} break;
		case TT_IDENT: {
			StrV ident = token.value.ident;
			if (strv_eq(ident, strv_from_cstr("abs"))) {
				Node *op1 = node_stack_pop(&node_stack);
				Node *n = create_unop_node(UNOP_ABS, op1);
				node_stack_push(&node_stack, n);
			} else {
				report_fmt(ERROR, "Unknown identifier \"" StrV_fmt "\"", StrV_printf_arg(ident));
				return 1;
			}
		} break;
		case TT_PLUS: {
			Node *op2 = node_stack_pop(&node_stack);
			Node *op1 = node_stack_pop(&node_stack);
			Node *n = create_binop_node(BINOP_ADD, op1, op2);
			node_stack_push(&node_stack, n);
		} break;
		case TT_MINUS: {
			Node *op2 = node_stack_pop(&node_stack);
			Node *op1 = node_stack_pop(&node_stack);
			Node *n = create_binop_node(BINOP_SUB, op1, op2);
			node_stack_push(&node_stack, n);
		} break;
		case TT_ASTERISK: {
			Node *op2 = node_stack_pop(&node_stack);
			Node *op1 = node_stack_pop(&node_stack);
			Node *n = create_binop_node(BINOP_MUL, op1, op2);
			node_stack_push(&node_stack, n);
		} break;
		case TT_SLASH: {
			Node *op2 = node_stack_pop(&node_stack);
			Node *op1 = node_stack_pop(&node_stack);
			Node *n = create_binop_node(BINOP_DIV, op1, op2);
			node_stack_push(&node_stack, n);
		} break;
		}
	}

	if (node_stack.len > 1) {
		report_fmt(ERROR, "%zu elements on the stack, but expected only 1", node_stack.len);
	}
	assert(node_stack.len == 1);
	Node *n = node_stack.elems[0];
	//print_node(*n, 0);

	node_number_t result = execute_node(n);

	printf("The result is " node_number_fmt "\n", result);

	return 0;

err:
	if (text.str) free((void *)text.str);

	return 1;
}
