# Reverse Polish Notation Calculator

A simple reverse polish notation calculator written in C.

| symbol | operation |
| :---: | :--- |
| `+` | addition |
| `-` | subtraction |
| `*` | multiplication |
| `/` | division |
| `abs` | absolute value |

## Compile and run

```console
$ ./build.sh
$ ./rpnc examples/sample.rpnc
```
